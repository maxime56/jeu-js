class SimonButton { // la class qui permet de crée les buttons
    constructor(id, litColor, color) {

        /**
         * @param {string} color
         * @param {string} id
         * @param {string} litColor
         */
        this.color = color;
        this.id = id;
        this.litColor = litColor;
        this.highlight = function () {
            document.getElementById(id).style.background = litColor;        // permet de changer la couleur avec celle du lit colot
            setTimeout(function () {
                document.getElementById(id).style.background = color;       // rechange la couleur par celle de base ca permet de crée une sorte de "flash"
            }, vitesseFlash);  // timier qui permet d'ajuster la vitesse du flash
            // 300
        }
    }
}
// ajoute des instances de la class SimonButton dans un array;
let buttons = [
    new SimonButton("red", "red", "darkred"),                             // buttons[0];
    new SimonButton("yellow", "yellow", "darkorange"),                   // buttons[1];
    new SimonButton("blue", "lightblue", "darkblue"),                   // buttons[2];
    new SimonButton("green", "lightgreen", "darkgreen")                 // buttons[3];
];


let easyButton = document.querySelector('#easy');
let mediumButton = document.querySelector('#medium');
let hardButton = document.querySelector('#hard');


let vitesseFlash = 300; // la variable qui va influencer sur la vitesse du flash
let vitesseSequence = 650; // ajouter de la difficulter au fur a mesure que le jeux augmente
let sequence = []; // un array vide qui va stocker nos sequences crée par un par un maths random et push
let round = 0; // le round va nous permettre de push une sequence à chaque fois qu'il augmente
let userTurn = false; // qui va permettre de savoir si c'est le tour du joueur
let sequenceIterator = 0;
playing = false; //et pour savoir si le jeu est lancé


window.onload = function () {

    // track le boutton newGame
    document.querySelector("#newGame").onclick = function () {
        document.querySelector("#newGame").value = "New Game";
        reset();
        playing = true;
        computerTurn();
    }



    for (let i = 0; i < buttons.length; i++) {

        document.getElementById(buttons[i].id).onclick = function () { // avec la boucle ca track les instances de l'array button 
            if (playing) {
                if (!userTurn) {
                    alert("Wait the end of the sequence.");
                } else {
                    if (buttons[sequence[sequenceIterator++]].id != this.id) { // si l'id appelé correspond pas c'est la fin du jeu
                    gameOver();  
                    }
                    if (sequenceIterator === sequence.length && playing) {
                // on vérifie si le joueur a bon par "défaut" avec le condition du game over pour avoir une condition qui 
                // permet d'avoir "gagner" et passer au prochain tour on regarde la sequenceIterator qui va être égal à la séquence
                        sequenceIterator = 0;
                        document.querySelector("#counter").innerHTML = ++round;
                        computerTurn();
                        increaseDifficult();
                    }
                }
            } else {
                alert("Click to the button start for play.");
            }
        }
    }
}


function computerTurn() { // tour de l'ordi
    userTurn = false;
    extendSequence(1); // function qui ajoute les séquences
    showSequence();
}

function reset() { // comme son nom reset le jeux
    sequence = [];
    round = 0;
    sequenceIterator = 0;
    document.querySelector("#counter").innerHTML = round;
}

function gameOver() {  // fin du jeux 
    playing = false;
    alert("Game Over. Try again.");
}

function extendSequence(number) {
    for (i = 0; i < number; i++) {
        sequence.push(Math.floor(Math.random() * 4));      // ajoute un nombre à la séquence
    }
}

function showSequence() {
    let i = 0;
    sequenceLoop();

    function sequenceLoop() {
        setTimeout(function () {
            buttons[sequence[i++]].highlight();  // 
            console.log(buttons[sequence[i]]);// log qui affiche les sequences
            console.log(sequenceIterator);
            if (i < sequence.length) {
                sequenceLoop();
            } else {
                userTurn = true;
            }
        }, vitesseSequence); // timer pour ajuster la prochaine sequence 
        //500
    }
}

function increaseDifficult() {      //permet d'augmenter la vitesse de la sequence à chaque condition remplis
    if (round == 4) {
        vitesseSequence -= 75;
    }
    if (round == 7) {
        vitesseSequence -= 75;
    }
    if (round == 10) {
        vitesseSequence -= 75;

    }
}
// difficulty button

easyButton.addEventListener('click', function () {
    if (!playing) {
        vitesseFlash = 300;
        vitesseSequence = 650;
    } if (round == 0) {
        vitesseFlash = 300;
        vitesseSequence = 650;
    } else {
        alert("Start a new game for change the difficulty.");
    }
});

mediumButton.addEventListener('click', function () {
    if (!playing) {
        vitesseFlash = 300;
        vitesseSequence = 550;
    } if (round == 0) {
        vitesseFlash = 300;
        vitesseSequence = 550;
    } else {
        alert("Start a new game for change the difficulty.");
    }
});

hardButton.addEventListener('click', function () {
    if (!playing) {
        vitesseFlash = 250;
        vitesseSequence = 450;
    } if (round == 0) {
        vitesseFlash = 250;
        vitesseSequence = 450;
    } else {
        alert("Start a new game for change the difficulty.");
    }
});

