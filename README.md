# Projet jeu JS

Durée du projet : 3~ semaines.



Projet crée un jeu: Simon game.



Le simon game est un jeu où il faut répéter les séquences crée par le jeu sans se tromper.

La difficulté augmente avec le nombre de séquence 



J'ai décidé de reproduire ce jeu car j'ai trouvé ca intéressant la manière dont marche le jeu, en créant et en ajoutant des séquences à chaque tours et de faire attention à la rapidité du flash





### Maquette

![simonGame](maquette/simonGame.svg)

Résultat : ![jeu](screenCode/jeu.png)



### User storie

En tant que qu'utilisateur je veux pouvoir changer la difficulté pour adapter le niveau.

En tant qu'utilisateur je veux voir mon score pour voir ma progression.

En tant que qu'utilisateur je veux pouvoir répéter les séquences joué par la "machine".

### Techo 

HTML5 pour crée les éléments que j'aurais besoin pour mon jeu.

CSS3 pour donner la forme à mes éléments , la taille et leurs couleurs. 

BOOTSTRAP pour agencé les éléments et rendre le jeu un minimum responsive.

JAVASCRIPT pour crée les fonctionnalités qui vont permettre au jeu de marcher.

### UML 



![ClassDiagram1SimonGame](UML/ClassDiagram1SimonGame.svg)

### User case

![](UML/UseCaseDiagram1SimonGame.svg)



### Fonctionnalité



Au chargement de la page avec une boucle on assigne une function aux buttons, on regarde si c'est le tour du joueur si oui on alerte le joueur, 

sinon on regarde si les id correspond pas on lance la function qui arrête le jeu et si le joueur ne se trompe pas on regarde le sequenceIterator si il est égale à la sequence.length le joueur gagne.



  ![gameCode](screenCode/gameCode.png)